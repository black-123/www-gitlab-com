description: The DevOps platform is in place but is the team structure ready for
  prime time? Here’s what you need to consider when building a DevOps team.
canonical_path: /topics/devops/build-a-devops-team/
parent_topic: devops
file_name: build-a-devops-team
twitter_image: /images/opengraph/gitlab-blog-cover.png
title: Create the ideal DevOps team structure
header_body: A solid DevOps platform needs a solid DevOps team structure to
  achieve maximum efficiency. But finding the right balance in your DevOps team
  is not a one-size-fits-all proposition.
body: >-
  A solid DevOps platform needs a solid DevOps team structure to achieve maximum
  efficiency. But finding the right balance in your DevOps team is not a
  one-size-fits-all proposition.


  Several factors come into play when it comes to team structure:


  * Existing silos: Are there product sets/teams that work independently?

  * Technical leadership: Are group managers set up to achieve DevOps goals?

  * Changing roles: Ops tasks have bled into dev roles, security teams are working with everyone, and technology is changing. Expect to regularly re-evaluate everything.

  * Continuous improvement: A DevOps team will never be a “one and done.” Iteration will be required.


  ## Types of silos


  Management consultant Matthew Skelton writes about a [number of different DevOps scenarios](https://blog.matthewskelton.net/2013/10/22/what-team-structure-is-right-for-devops-to-flourish/) in great detail, but we’ll discuss just a few of the silos he mentions specifically and how they impact an organization.


  ## Dev and ops are completely separate


  Skelton refers to this as a classic “throw it over the wall” team structure and, as implied, it’s not the most effective DevOps strategy. Both teams work in their bubbles and lack visibility into the workflow of the other team. This complete separation lacks collaboration, visibility, and understanding – vital components of what effective DevOps should be. What happens is essentially blame-shifting: "We don’t know what they are doing over there, we did our part and now it's up to them to complete it," and so on.


  ## DevOps middleman


  In this team structure, there are still separate dev and ops teams, but there is now a “DevOps” team that sits between, as a facilitator of sorts. This is not necessarily a bad thing and Skelton stresses that this arrangement has some use cases. For example, if this is a temporary solution with the goal being to make dev and ops more cohesive in the future, it could be a good interim strategy.


  ## Ops stands alone


  In this scenario, dev and DevOps are melded together while ops remains siloed. Organizations like this still see ops as something that supports the initiatives for software development, not something with value in itself. Organizations like this suffer from basic operational mistakes and could be much more successful if they understand the value ops brings to the table.


  ## What can DevOps team leadership do?


  To break down DevOps team silos requires leadership at all levels. Start by asking each group to surface the major areas of friction and then identify leaders in each group – dev, ops, security, test. Each leader should work individually and together on all of the friction points.


  The importance of communication can't be overstated: Teams need to hear regular feedback about all aspects of their roles.


  It might also be helpful to insert "champions" into struggling groups; they can model behaviors and language that facilitate communication and collaboration.


  ## DevOps roles are blurring


  Technology advances from multicloud to microservices and containers also play a role when it comes to defining the right DevOps team structure. In our [2020 Global DevSecOps Survey](https://about.gitlab.com/developer-survey/), 83% of respondents said their teams are releasing code more quickly but they also told us their roles were changing, dramatically in some cases.


  Devs today are creating, monitoring, and maintaining infrastructures, roles that were traditionally the province of ops pros. Ops are spending more time managing cloud services, while security team members are working on cross-functional teams with dev and ops more than ever before.


  Obviously the software development lifecycle today is full of moving parts, meaning that defining the right structure for a DevOps team will remain fluid and in need of regular re-evaluation.


  ## Remember to iterate


  At GitLab [iteration](/handbook/values/#iteration) is one of our core values. And it’s something we practice a lot when it comes to our own DevOps team structure. Since GitLab is a complete [DevOps platform](/solutions/devops-platform/) delivered as a single application, our dev teams are organized into stages (e.g. [Verify](https://about.gitlab.com/handbook/engineering/development/ops/verify/), etc.) because these would be separate products at any other company and require their own autonomy. We also have other functional DevOps groups besides "Dev" that manage other aspects of our product. We have a [reliability group](https://about.gitlab.com/handbook/engineering/infrastructure/team/reliability/) that manages uptime and reliability for GitLab.com, a [quality department](https://about.gitlab.com/handbook/engineering/quality/), and a [distribution team](https://about.gitlab.com/handbook/engineering/development/enablement/distribution/), just to name a few. The way that we make all these pieces fit together is through our commitment to transparency and our visibility through the entire SDLC. But we also tweak (i.e. iterate on) this structure regularly to make everything work.


  The bottom line: Plan to build your DevOps team, and then re-think it, and re-think it some more. The benefits in faster code releases and happier team members will make it worthwhile.
cta_banner:
  - title: Mapping the DevSecOps Landscape
    cta:
      - url: https://about.gitlab.com/developer-survey/
        text: Get the Full Report
    body: This year, over 3,650 respondents from 21 countries spoke about their
      DevOps successes, challenges, and ongoing struggles.
resources:
  - title: The developer role is changing. Here's what to expect
    url: https://about.gitlab.com/blog/2020/10/20/software-developer-changing-role/
    type: Blog
  - title: Why you need a security champions program
    url: https://about.gitlab.com/blog/2020/10/14/why-security-champions/
    type: Blog
  - title: Advance DevOps with communication and collaboration
    url: https://about.gitlab.com/blog/2020/11/23/collaboration-communication-best-practices/
    type: Blog
