---
layout: handbook-page-toc
title: "Competitor Evaluations"
description: "Competitor evaluations help us understand how a competing product addresses the Jobs-To-Be-Done that our product also tries to address."
---

#### On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


## What are competitor evaluations?

Competitor evaluations help us understand how a competing product addresses the Jobs-To-Be-Done that our product also tries to address. Competitor Evaluations are different from a competitor analysis, which compares business models and strategic positioning in the market (think SWOT analysis). During a competitor evaluation, we're evaluating solutions, not companies.

In some cases, a competing company may solve a narrower aspect of DevSecOps than GitLab. In contrast, another company might have a portfolio of solutions that add up to the full breadth of GitLab. 

## Performing a competitor evaluation

### Part I
1. Create a new UX Research issue using [template link]
1. Review the evaluation heuristics with your counterparts and make sure they are understood
1. Identify the Jobs To Be Done and scenarios that you will use for the evaluation

### Part II
1. Sign up for the service(s)
1. Find a partner to ensure that your observations are on-point and your notes make sense
1. Duplicate the [spreadsheet template](https://docs.google.com/spreadsheets/d/1rtJV4a3IW-2jpq4Ifz3GBKcT3xikmZRecQVeBGJ3yz8/edit#gid=0) and evaluate with the heuristics
1. Show your evaluation experience with screen recordings instead of screenshots
1. Upload your recording(s) to GitLab Unfiltered and mark as Private
1. Summarize your insights into the UX Research issue

## Heuristics

### Usability

1. Visibility of system status: Communicating the current state allows users to feel in control of the system, take appropriate actions to reach their goal, and ultimately trust the brand. (source: [NN/g](https://www.nngroup.com/articles/visibility-system-status/))
1. Flexibility of use: Shortcuts (unseen by the novice user) speed up the interaction for the experts such that the system can cater to inexperienced and experienced users. (source: [NN/g](https://www.nngroup.com/articles/flexibility-efficiency-heuristic/))
1. User control and freedom: Allow people to exit a flow or undo their last action and go back to the system’s previous state. (source: [NN/g](https://www.nngroup.com/articles/user-control-and-freedom/))
1. Recognize, diagnose, recover from errors: Make error messages visible, reduce the work required to fix the problem, and educate users along the way. (source: [NN/g](https://www.nngroup.com/articles/error-message-guidelines/))

### Laws and tenets (source: [uitraps.com](https://uitraps.com/))

1. Protective: I don’t lose my data
1. Forgiving: I can undo my actions
1. Understandable: I know what I can do
1. Efficient: I take fewer steps and process less information

### Characteristics

1. Real-time user interface (see: [The future of MRs: Real-time collaboration](https://about.gitlab.com/blog/2019/12/19/future-merge-requests-realtime-collab/))
1. A tool the whole team can use (see: [GitLab Direction #personas](https://about.gitlab.com/direction/#personas))
1. Minimal setup required (see: [GitLab Product Principles #convention-over-configuration](https://about.gitlab.com/handbook/product/product-principles/#convention-over-configuration))
1. Learnability and support for new users
1. For technical areas, documentation is easy to use

## Communicating the results

Try to show your evaluation experience with screen recordings instead of screenshots. Share your insights issue with relevant groups and at UX Showcase.
